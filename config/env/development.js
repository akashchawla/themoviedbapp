'use strict';

module.exports = {
    db: 'mongodb://tmdbadmin:tmdbadminpwd@ds161121.mlab.com:61121/themoviedbapp',
    app: {
        title: 'The Movie DB App - Development Environment'
    },
    mailer: {
        from: process.env.MAILER_FROM || 'MAILER_FROM',
        options: {
            service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
            auth: {
                user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
                pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
            }
        }
    },
    externalServices : {
        themovieDb : {
            HOST : 'https://api.themoviedb.org',
            IMAGE_1x_HOST : 'https://image.tmdb.org/t/p/w640',
            IMAGE_2x_HOST : 'https://image.tmdb.org/t/p/w1280',
            CREATE_TOKEN : '/3/authentication/token/new',
            VALIDATE_WITH_TOKEN : '/3/authentication/token/validate_with_login',
            SESSION_CREATE : '/3/authentication/session/new',
            ACCOUNT_DETAIL : '/3/account',
            MULTI_SEARCH : '/3/search/multi'
        }
    }
};

