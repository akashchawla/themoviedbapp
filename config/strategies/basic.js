'use strict';

/**
 * Module dependencies.
 */
var passport        = require('passport'),
    BasicStrategy   = require('passport-http').BasicStrategy,
    AuthCtrl        = require('../../app/controllers/auth.server.controller'),
    UserCtrl        = require('../../app/controllers/user.server.controller'),
    request         = require('request'),
    Q               = require('q'),
    config          = require('../../config/config');

module.exports = function() {
    // Using Basic strategy

    passport.use(new BasicStrategy(
        function(username, password, callback) {

            var promiseInstance = new Q(undefined);

            promiseInstance
                .then(function(){
                    return AuthCtrl.createAuthRequestToken();
                })
                .then(function(tokenDetails){
                    return AuthCtrl.validateToken(tokenDetails, username, password);
                })
                .then(function(tokenDetails){
                    return AuthCtrl.createSession(tokenDetails);
                })
                .then(function(accountSessionDetails){
                    return AuthCtrl.fetchUserDetails(accountSessionDetails);
                })
                .then(function(userSessionDetails){
                    callback(null, userSessionDetails);
                })
                .fail(function(err){
                    callback(err);
                });
        }
    ));
};
