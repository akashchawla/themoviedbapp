'use strict';

//Setting up route
angular.module('admin').config(['$stateProvider','localStorageServiceProvider', '$urlRouterProvider',
	function($stateProvider, localStorageServiceProvider, $urlRouterProvider) {

		// Admin state routing
        localStorageServiceProvider.setStorageType('sessionStorage');
		$stateProvider.
		state('login', {
			url: '/login',
			templateUrl: 'modules/admin/views/login.client.view.html',
            data: {
                requestAuthenticate: false
            }
		}).
/*		state('history', {
			url: '/history',
			templateUrl: 'modules/admin/views/history.client.view.html',
            data: {
                requestAuthenticate: true
            }
		}).*/
		state('home', {
			url: '/home',
			templateUrl: 'modules/admin/views/home.client.view.html',
                data: {
                    requestAuthenticate: true
                }
		});
	}
]);