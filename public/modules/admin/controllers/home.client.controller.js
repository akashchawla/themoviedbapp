'use strict';

angular.module('admin')
    .controller('HomeController', HomeController);

/*@ngInject*/

function HomeController ($scope, $modal, $timeout,User,StorageService,HttpService) {
    /*jshint validthis: true*/
    var vm = this,
        items;

    vm.init = function () {
        vm.user= StorageService.user();
    };

    vm.searchForResults = function(){


    };

    $scope.filterText = '';

    // Instantiate these variables outside the watch
    var tempFilterText = '',
        filterTextTimeout;
    $scope.$watch('searchText', function (val) {
        if (filterTextTimeout) {
            $timeout.cancel(filterTextTimeout);
        }

        tempFilterText = val;
        filterTextTimeout = $timeout(function() {
            vm.searchText = $scope.filterText = tempFilterText;
            if(tempFilterText && tempFilterText.length > 0){
                HttpService
                    .getData('/api/v1/search?q=' + vm.searchText, {
                        headers: {
                            'x-access-session' : StorageService.getUserSession(),
                            'Content-Type' : 'application/json'
                        }
                    })
                    .then(function (responseData) {
                        vm.searchResult = responseData.data;
                        StorageService.storeSearchResult(vm.searchResult.results);
                    }).catch(function (){
                        var results = StorageService.fetchSearchResult(tempFilterText);

                        vm.searchResult = {
                            results : results
                        };
                    });
            }

        }, 250); // delay 250 ms
    });

    vm.getImagePath = function(result){

        var defaultImageUrl = '/modules/core/img/brand/no-image-slide.png';
        switch(result.media_type){

            case 'movie' :
            case 'tv' :
                result.small_image_url = result.poster_path ? (result.image_host_1x + result.poster_path) : defaultImageUrl;
                result.large_image_url = result.poster_path ? (result.image_host_2x + result.poster_path) : defaultImageUrl;
                result.backdrop_url = result.backdrop_path ? result.image_host_1x + result.backdrop_path : defaultImageUrl;
                break;

            case 'person' :
                result.small_image_url = result.profile_path ? (result.image_host_1x + result.profile_path) : defaultImageUrl;
                result.large_image_url = result.profile_path ? (result.image_host_2x + result.profile_path) : defaultImageUrl;
                break;

            default :
                result.small_image_url = defaultImageUrl;
                result.large_image_url = defaultImageUrl;

        }
    };

    vm.showDetails = function (result) {
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/details.client.view.html',
            controller:'DetailsController',
            controllerAs: 'vm',
            resolve: {
                item: function () {
                    return result;
                }
            }
        });

        importedResult.result.then(function () {

        }).catch(function () {

        });
    };

    vm.showImage = function (result) {
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/imageView.client.view.html',
            controller:'ImageViewController',
            controllerAs: 'vm',
            resolve: {
                item: function () {
                    return result;
                }
            }
        });

        importedResult.result.then(function () {

        }).catch(function () {

        });
    };


}
