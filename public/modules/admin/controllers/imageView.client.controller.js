'use strict';
angular.module('admin')
    .controller('ImageViewController', ImageViewController);
/*@ngInject*/

function ImageViewController ($scope, HttpService, StorageService,$modalInstance, item ,$location) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;

    vm.loadImage = function () {
        vm.imageTitle = item.name || item.title;
        vm.imageUrl = item.large_image_url;
    };

    function modalClose () {
        $modalInstance.close();
    }
}

