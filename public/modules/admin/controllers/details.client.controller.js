'use strict';
angular.module('admin')
    .controller('DetailsController', DetailsController);
/*@ngInject*/

function DetailsController ($scope, HttpService, StorageService,$modalInstance, item ,$location) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;

    vm.initDetailModal = function () {
        vm.data = item;
    };

    function modalClose () {
        $modalInstance.close();
    }
}
