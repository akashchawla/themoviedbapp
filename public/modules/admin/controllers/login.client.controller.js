'use strict';

angular
    .module('admin')
    .controller('LoginController', LoginController);
/*@ngInject*/

function LoginController ($scope, $location, HttpService, $window, StorageService, $rootScope) {
    /*jshint validthis:true*/
    var vm = this;
    vm.doLogin = function (user, isValid) {
        var loginData = user;

        if(!isValid){
            return;
        }
        vm.isLoading = true;
        HttpService
            .postData('/api/v1/users/signin', {
                headers: {
                    'Authorization' : 'Basic ' + btoa(loginData.username + ':' + loginData.password),
                    'Content-Type' : 'application/json'
                },
                data :JSON.stringify({})
            })
            .then(function (data) {
                var userInfo = data.data;
                StorageService.setUserDetails(userInfo);
                $location.path('/home');
                vm.isLoading = false;
            }).catch(function (){
                $location.path('/login');
                vm.isLoading = false;
                vm.serverError = true;
            });
    };
}