'use strict';

angular.
    module('core')
.factory('StorageService', StorageService);
/*@ngInject*/
function StorageService (localStorageService, $window, $location){
    var storage ={};
    storage.user = function () {
        var user = localStorageService.get('user');

        var userInfo = {};
        try{
            userInfo = JSON.parse(user);
        }
        catch(err){
            userInfo = null;
        }
        return userInfo;
    };

    storage.setUserDetails = function (userDetails) {
        localStorageService.set('session_id', userDetails.session_id);

        userDetails.session_id = undefined;
        localStorageService.set('user', JSON.stringify(userDetails));
    };
    storage.getUserSession = function () {
        return localStorageService.get('session_id');
    };
    storage.removeUserSession = function () {
        localStorageService.remove('session_id');
        localStorageService.remove('user');
        localStorageService.remove('search');
        $location.path('/login');
    };

    storage.storeSearchResult =  function(results) {
        var searchResults = localStorageService.get('search');

        var searchData = {};
        if(searchResults) {
            try {
                searchData = JSON.parse(searchResults);
            }
            catch (err) {
                searchData = {};
            }
        }

        results.forEach(function(result){
            var id = result.id;
            searchData[id] = result;
        });

        localStorageService.set('search', JSON.stringify(searchData));
    };

    storage.fetchSearchResult = function(searchText){
        var searchResults = localStorageService.get('search');

        var searchData = {};
        if(searchResults) {
            try {
                searchData = JSON.parse(searchResults);
            }
            catch (err) {
                searchData = {};
            }
        }

        searchResults = [];
        var searchTextExp = new RegExp(searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'), 'i');
        Object.keys(searchData).forEach(function(key){
            var haystackObj = searchData[key];

            var haystack = haystackObj.name || haystackObj.title;

            if(searchTextExp.test(haystack)){
                searchResults.push(haystackObj);
            }

        });

        return searchResults;
    };

    return storage;
}