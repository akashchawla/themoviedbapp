'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider', 'localStorageServiceProvider',
	function($locationProvider, $httpProvider, localStorageServiceProvider) {
		$locationProvider.hashPrefix('!');
        localStorageServiceProvider
            .setPrefix('gslite');
        $httpProvider.interceptors.push(function($timeout, $injector){
            var $http, $state;
            $timeout(function(){
                $http = $injector.get('$http');
                $state = $injector.get('$state');
            }, 200);
            return {
                request: function(config) {
                    return config;
                },
                requestError: function (rejection) {
                    return rejection;
                }
            };
        });
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
//--> Check if user is logged in
angular.module(ApplicationConfiguration.applicationModuleName)
    .run(['$rootScope', '$location', 'StorageService',
        function($rootScope, $location, StorageService){
        //----> check the state change of route
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            var currentUser = StorageService.user(),
            isAuthenticateUrl = toState.data ? toState.data.requestAuthenticate : false;
            if(isAuthenticateUrl && !currentUser){
                event.preventDefault();
                $rootScope.$evalAsync(function() {
                    $location.path('/login');
                    $rootScope.isHeaderActive = false;
                });
            }

            $rootScope.isHeaderActive = toState.name !== 'login';
    });

}]);
