'use strict';

/**
 * Module dependencies
 */
var should              = require('should'),
    request             = require('supertest'),
    app                 = require('../../../server'),
    agent               = request.agent(app),
    mongoose            = require('mongoose'),
    testUtils           = require('../../utility/unit-tests.utility'),
    constants           = require('../../utility/constants.utility'),
    configCredentials   = require('../../../config/config').signInCredentials,
    User                = mongoose.model('User');


describe('User routes unit tests',function(){

    describe('User SignIn API test cases',function(){

        var user;
        beforeEach(function(done){

            user = new User(constants.unitTests.TEST_USER_OBJECT);

            user.save(function(errSave){
                done(errSave);
            });
        });

        it('should return 401 UnAuthorized if wrong username is provided in Basic Authorization Header',function(done){

            var authorizationHeader = 'Basic ',
                emailPwdEncoded = new Buffer('wrongUser:'+ configCredentials.webAppPassWord).toString('base64'),
                credentials = {
                    email: constants.unitTests.TEST_USER_OBJECT.email,
                    password: constants.unitTests.TEST_USER_OBJECT.password
                };

            authorizationHeader = authorizationHeader + emailPwdEncoded;

            agent.post('/api/v1/users/signin')
                .set('Authorization', authorizationHeader)
                .send(credentials)
                .expect(401)
                .end(function(signInErr, signInRes) {
                    try { // To be able to get the assert failures
                        if(signInErr) { done(signInErr); }

                        //Set assertions
                        signInRes.text.should.equal('Unauthorized');
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 401 UnAuthorized if wrong password is provided in Basic Authorization Header',function(done){
            var authorizationHeader = 'Basic ',
                emailPwdEncoded = new Buffer(configCredentials.webAppUserName + ':wrongPassword').toString('base64'),
                credentials = {
                    email: constants.unitTests.TEST_USER_OBJECT.email,
                    password: constants.unitTests.TEST_USER_OBJECT.password
                };

            authorizationHeader = authorizationHeader + emailPwdEncoded;

            agent.post('/api/v1/users/signin')
                .set('Authorization', authorizationHeader)
                .send(credentials)
                .expect(401)
                .end(function(signInErr, signInRes) {
                    try { // To be able to get the assert failures
                        if(signInErr) { done(signInErr); }

                        //Set assertions
                        signInRes.text.should.equal('Unauthorized');
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 401 UnAuthorized if wrong email is provided in Request',function(done){
            var authorizationHeader = 'Basic ',
                emailPwdEncoded = new Buffer(configCredentials.webAppUserName + ':' + configCredentials.webAppPassWord).toString('base64'),
                credentials = {
                    email: constants.unitTests.TEST_USER_OBJECT.email+'1',
                    password: 'wrongPassword'
                };

            authorizationHeader = authorizationHeader + emailPwdEncoded;

            agent.post('/api/v1/users/signin')
                .set('Authorization', authorizationHeader)
                .send(credentials)
                .expect(401)
                .end(function(signInErr, signInRes) {
                    try { // To be able to get the assert failures
                        if(signInErr) { done(signInErr); }

                        //Set assertions
                        signInRes.text.should.equal(constants.messages.EMAIL_OR_PASSWORD_INCORRECT);
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 401 UnAuthorized if wrong password is provided in request',function(done){
            var authorizationHeader = 'Basic ',
                emailPwdEncoded = new Buffer(configCredentials.webAppUserName + ':' + configCredentials.webAppPassWord).toString('base64'),
                credentials = {
                    email: constants.unitTests.TEST_USER_OBJECT.email,
                    password: 'wrongPassword'
                };

            authorizationHeader = authorizationHeader + emailPwdEncoded;

            agent.post('/api/v1/users/signin')
                .set('Authorization', authorizationHeader)
                .send(credentials)
                .expect(401)
                .end(function(signInErr, signInRes) {
                    try { // To be able to get the assert failures
                        if(signInErr) { done(signInErr); }

                        //Set assertions
                        signInRes.text.should.equal(constants.messages.EMAIL_OR_PASSWORD_INCORRECT);
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 200 along with AccessToken if correct credentials are provided in Basic Authorization Header',function(done){

            var authorizationHeader = 'Basic ',
                emailPwdEncoded = new Buffer(configCredentials.webAppUserName + ':' + configCredentials.webAppPassWord).toString('base64'),
                credentials = {
                    email: constants.unitTests.TEST_USER_OBJECT.email,
                    password: constants.unitTests.TEST_USER_OBJECT.password
                };

            authorizationHeader = authorizationHeader + emailPwdEncoded;

            agent.post('/api/v1/users/signin')
                .set('Authorization', authorizationHeader)
                .send(credentials)
                .expect(200)
                .end(function(signInErr, signInRes) {
                    try { // To be able to get the assert failures
                        if(signInErr) { done(signInErr); }

                        //Set assertions
                        signInRes.text.should.match(function(it){return it.should.not.be.empty;});
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        afterEach(function (done) {
            User.remove().exec(function(err){
                done(err);
            });
        });
    });

    describe('User Token Authentication and User listing API',function(){

        var accessToken;
        before(function(done){
            accessToken = null;
            testUtils.createTestUserAndLoginForAccessToken(function(err,user){
                accessToken = user ? user.accessToken : null;
                done(err);
            });
        });

        it('should return 401 if wrong access token is passed is request header',function(done){

            var wrongAccessToken = accessToken + 'wrong';
            agent.get('/api/v1/users')
                .set('x-access-token', wrongAccessToken)
                .expect(401)
                .end(function(getUsersErr, getUsersRes) {
                    try { // To be able to get the assert failures
                        if(getUsersErr) { done(getUsersErr); }

                        //Set assertions
                        getUsersRes.text.should.equal('Unauthorized');
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 401 if access token has expired',function(done){

            var accessTokenSplits = accessToken.split('.');
            var payload = JSON.parse(new Buffer(accessTokenSplits[1], 'base64').toString('ascii'));
            payload.exp = payload.exp - 3600;
            accessTokenSplits[1] = new Buffer(JSON.stringify(payload)).toString('base64');

            var expiredAccessToken = accessTokenSplits.join('.');

            agent.get('/api/v1/users')
                .set('x-access-token', expiredAccessToken)
                .expect(401)
                .end(function(getUsersErr, getUsersRes) {
                    try { // To be able to get the assert failures
                        if(getUsersErr) { done(getUsersErr); }

                        //Set assertions
                        getUsersRes.text.should.equal('Unauthorized');
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should successfully return list of users',function(done){

            agent.get('/api/v1/users')
                .set('x-access-token', accessToken)
                .expect(200)
                .end(function(getUsersErr, getUsersRes) {
                    try { // To be able to get the assert failures
                        if(getUsersErr) { done(getUsersErr); }

                        //Set assertions
                        getUsersRes.body.should.match(function(it){ return it.should.be.Array;});
                        getUsersRes.body.length.should.equal(1);
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        after(function (done) {
            testUtils.deleteTestUser(function(err){
                done(err);
            });
        });

    });

    describe('User sign out API test cases',function(){

        var accessToken;
        before(function(done){
            accessToken = null;
            testUtils.createTestUserAndLoginForAccessToken(function(err,user){
                accessToken = user ? user.accessToken : null;
                done(err);
            });
        });

        it('should return 401 if wrong access token is passed is request header',function(done){

            var wrongAccessToken = accessToken + 'wrong';
            agent.get('/api/v1/users/signout')
                .set('x-access-token', wrongAccessToken)
                .expect(401)
                .end(function(signOutErr, signOutRes) {
                    try { // To be able to get the assert failures
                        if(signOutErr) { done(signOutErr); }

                        //Set assertions
                        signOutRes.text.should.equal('Unauthorized');
                        done();
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        it('should return 204 and sign out successfully',function(done){

            agent.get('/api/v1/users/signout')
                .set('x-access-token', accessToken)
                .expect(204)
                .end(function(signOutErr, signOutRes) {
                    try { // To be able to get the assert failures
                        if(signOutErr) { done(signOutErr); }

                        //Set assertions
                        signOutRes.text.should.match(function(it){ return it.should.be.empty;});
                        agent.get('/api/v1/users/signout')
                            .set('x-access-token', accessToken)
                            .expect(401)
                            .end(function(logOutErr, logOutRes) {

                                if(logOutErr) { done(logOutErr); }

                                //Set assertions
                                logOutRes.text.should.equal('Unauthorized');
                                done();
                            });
                    }
                    catch (err) {
                        done(err);
                    }
                });
        });

        after(function (done) {
            testUtils.deleteTestUser(function(err){
                done(err);
            });
        });

    });
});
