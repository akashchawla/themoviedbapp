'use strict';

/*
  This files contains the util method for Unit tests
 */

(function(testUtils){

    var mongoose  = require('mongoose'),
        User      = mongoose.model('User'),
        userCtrl  = require('../controllers/user.server.controller'),
        constants = require('../utility/constants.utility'),
        domain    = require('domain');

    testUtils.createTestUserAndLoginForAccessToken = function(callback){

        var d = domain.create();

        d.on('error',function(err){
            callback(err,null);
        });

        var user = new User(constants.unitTests.TEST_USER_OBJECT);

        user.save(d.intercept(function(addedUser) {
           userCtrl.signInUserWithAcessToken(addedUser).then(function(userWithAccessToken){
                callback(null,userWithAccessToken);
           }).catch(function(errSignIn){
                callback(errSignIn,null);
           });
        }));
    };

    testUtils.deleteTestUser = function(callback){

        var testUserEmail = constants.unitTests.TEST_USER_OBJECT.email;

        User.remove({email:testUserEmail}).exec(function(err){
            callback(err);
        });
    };
}(module.exports));

