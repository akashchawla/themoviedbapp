'use strict';

var csvtojson = require('csvtojson' ),
    deferred = require('deferred');

var CommonUtil = function (){};

CommonUtil.prototype.readCsv = function (csvFileName, cb) {
    var Converter       = csvtojson.core.Converter,
        fs              = require('fs' ),
        fileStream      = fs.createReadStream(csvFileName ),
        param           = {},
        csvConverter    = new Converter(param ),
        defer           = deferred(),
        rawRows         = [],
        validRows       = [],
        failedRows      = [];

    csvConverter.on('record_parsed', function (resultRow, rawRow, rowIndex) {
        if(resultRow) {
            cb(resultRow).then(function (userInfo) {
                validRows.push(resultRow);
            }, function (err) {
                failedRows.push(resultRow);
            });
        }
        //if(rawRow){
        //    rawRows.push(rawRow);
        //}
    });
    csvConverter.on('end_parsed', function (jsonObj) {
        defer.resolve(jsonObj);
        fs.unlink(csvFileName, function (err) {
            if (err) throw err;
            console.log('successfully deleted');
        });
    });
    fileStream.pipe(csvConverter);

    return defer.promise;
};

exports.commonUtil = new CommonUtil();