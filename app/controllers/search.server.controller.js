'use strict';

/*
 Module Dependencies
 */
var mongoose    = require('mongoose'),
    domain      = require('domain'),
    request     = require('request'),
    Q           = require('q'),
    config      = require('../../config/config'),
    _           = require('lodash');


var userController = {

    searchResults : function(req, res){
        var searchText = req.query.q || '';
        searchText = searchText.trim();

        var noResultResponse = {
            page: 1,
            total_pages: 1,
            total_results: 0,
            searchText: searchText,
            results: []
        };

        if(_.isEmpty(searchText)){
            res.status(422).send(noResultResponse);
        }else {


            var searchResultOpts = {
                method: 'GET',
                url: config.externalServices.themovieDb.HOST + config.externalServices.themovieDb.MULTI_SEARCH,
                qs: {
                    api_key: config.apiKey,
                    query : searchText
                },
                json: true,
                forever: true
            };

            request(searchResultOpts, function (err, response, responseBody) {
                if (err || !response || response.statusCode !== 200) {
                    res.status(422).send(noResultResponse);
                } else {

                    _.forEach(responseBody.results,function(result){
                        result.image_host_1x = config.externalServices.themovieDb.IMAGE_1x_HOST;
                        result.image_host_2x = config.externalServices.themovieDb.IMAGE_2x_HOST;
                    });

                    var result = _.merge(noResultResponse,responseBody);
                    res.json(result);
                }
            });
        }
    }
};

module.exports = userController;