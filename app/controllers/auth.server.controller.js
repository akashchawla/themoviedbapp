'use strict';

/*
 Module Dependencies
 */
var passport = require('passport'),
    request  = require('request'),
    Q        = require('q'),
    _        = require('lodash'),
    config   = require('../../config/config');

var authController = {

    /*
     Authenticate sign in API with basic authentication strategy
     */
    signInAuthentication : function(req, res, next){

        passport.authenticate('basic', function(err, user, info){
            if (err || !user) {
                res.status(400).send(info);
            } else {
                // Remove sensitive data before login
                user.password = undefined;
                user.salt = undefined;

                req.login(user, function(err) {
                    if (err) {
                        res.status(400).send(err);
                    } else {
                        req.user = user;
                        next();
                    }
                });
            }
        })(req, res, next);

    },

    /*
     Authenticate other APIs with custom access-token authentication strategy
     */
    tokenAuthentication :function(req, res, next){

        passport.authenticate('token',function(err, user, info){
            if (err || !user) {
                res.status(400).send(info);
            } else {
                // Remove sensitive data before login
                user.password = undefined;
                user.salt = undefined;

                req.login(user, function(err) {
                    if (err) {
                        console.log('Error',err);
                        res.status(400).send(err);
                    } else {
                        req.user = user;
                        next();
                    }
                });
            }
        })(req, res, next);

    } ,


    createAuthRequestToken : function(){
        var deferred = Q.defer();

        var createRequestTokenOpts = {
            method: 'GET',
            url: config.externalServices.themovieDb.HOST + config.externalServices.themovieDb.CREATE_TOKEN,
            qs: {api_key: config.apiKey},
            json: true,
            forever:true
        };

        request(createRequestTokenOpts, function(err, response, responseBody){
            if(err || !response || response.statusCode !== 200){
                deferred.reject(new Error('An error occurred while generating token'));
            }else{
                deferred.resolve(responseBody);
            }
        });

        return deferred.promise;
    },

    validateToken : function(tokenDetails, userName, password){

        var deferred = Q.defer();

        var validateTokenOpts = {
            method: 'GET',
            url: config.externalServices.themovieDb.HOST + config.externalServices.themovieDb.VALIDATE_WITH_TOKEN,
            qs: {
                username: userName,
                password: password,
                api_key: config.apiKey,
                request_token: tokenDetails.request_token
            },
            json: true,
            forever:true
        };

        request(validateTokenOpts, function(err, response, responseBody){
            if(err || !response || response.statusCode !== 200){
                deferred.reject(new Error('An error occurred while validating token'));
            }else{
                deferred.resolve(tokenDetails);
            }
        });

        return deferred.promise;

    },

    createSession : function(tokenDetails){

        var deferred = Q.defer();

        var validateTokenOpts = {
            method: 'GET',
            url: config.externalServices.themovieDb.HOST + config.externalServices.themovieDb.SESSION_CREATE,
            qs: {
                api_key: config.apiKey,
                request_token: tokenDetails.request_token
            },
            json: true,
            forever:true
        };

        request(validateTokenOpts, function(err, response, responseBody){
            if(err || !response || response.statusCode !== 200){
                deferred.reject(new Error('An error occurred while generating session for user'));
            }else{
                deferred.resolve(_.merge(tokenDetails, responseBody));
            }
        });

        return deferred.promise;

    },

    fetchUserDetails : function(accountSessionDetails){

        var deferred = Q.defer();

        var validateTokenOpts = {
            method: 'GET',
            url: config.externalServices.themovieDb.HOST + config.externalServices.themovieDb.ACCOUNT_DETAIL,
            qs: {
                api_key: config.apiKey,
                session_id: accountSessionDetails.session_id
            },
            json: true,
            forever:true
        };

        request(validateTokenOpts, function(err, response, responseBody){
            if(err || !response || response.statusCode !== 200){
                deferred.reject(new Error('An error occurred while fetching user details'));
            }else{
                deferred.resolve(_.merge(accountSessionDetails, responseBody));
            }
        });

        return deferred.promise;
    }

};

module.exports = authController;
/*
exports.createJwtAccessToken = function(jwtTokenPayload){

    var currentDate = new Date();

    /!*!//Getting current time in Unix timestamp
    var currentUnixTime = Math.round(currentDate.getTime() / 1000);

    // “Issued at” time, in Unix time, at which the token was issued
    jwtTokenPayload.iat = currentUnixTime;

    // “Not before” time that identifies the time before which the JWT must not be accepted for processing
    jwtTokenPayload.nbf = currentUnixTime;
    *!/

    // Token expiration time defined in Unix time
    currentDate.setMinutes(currentDate.getMinutes() + config.accessToken.expirationInMinutes);
    jwtTokenPayload.exp  = Math.round(currentDate.getTime() / 1000);

    //Encoding the token payload with the secret key
    return jwt.encode(jwtTokenPayload, config.accessToken.secretKey);
};

/!*
 Verifies whether the access token has been expired or not
 *!/
exports.verifyTokenExpiration = function(accessToken){
    var decodedPayload = jwt.decode(accessToken, config.accessToken.secretKey),
        currentDate = new Date();
    var currentUnixTime = Math.round(currentDate.getTime() / 1000);

    return currentUnixTime < decodedPayload.exp;
};
*/



