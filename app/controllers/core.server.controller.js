'use strict';

/**
 * Module dependencies.
 */
var formidable      = require('formidable' ),
    util            = require('util' ),
    mongoose        = require('mongoose'),
    User            = mongoose.model('User');

exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req
	});
};
