'use strict';

/*
 Module Dependencies
 */
var mongoose    = require('mongoose'),
    User        = mongoose.model('User'),
    domain      = require('domain'),
    _           = require('lodash');


var userController = {

    signIn : function(req, res){
        var userInfo = req.user;
        var d = domain.create();

        d.on('error',function(err){
            return res.status(500).send(err);
        });

        User.findOne({ username: userInfo.username }, d.intercept(function (user) {
            // No user found with that username
            if (!user) {

                var userModelData = {
                    username : userInfo.username,
                    name : userInfo.name,
                    user_id : userInfo.id,
                    accessToken : userInfo.request_token,
                    sessionId : userInfo.session_id,
                    tokenValidTill : userInfo.expires_at
                };

                user = new User(userModelData);
            }else{
                user.accessToken = userInfo.request_token;
                user.tokenValidTill = userInfo.expires_at;
                user.sessionId = userInfo.session_id;
            }

            user.save(d.intercept(function (savedUser) {
                res.jsonp(userInfo);
            }));
        }));
    },

    signOut : function(req, res){
        var user = req.user;

        var d = domain.create();
        d.on('error',function(err){
            res.status(500).send(err);
        });

        var updateExpr = {
            $set: {'accessToken': '', 'session_id' : '', 'tokenValidTill' : null}
        };

        User.findByIdAndUpdate(user._id,updateExpr)
            .exec(d.intercept(function(savedUser){
                res.status(204).send();
            }));
    }
};

module.exports = userController;