'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    constants = require('../utility/constants.utility'),
    Schema = mongoose.Schema;

/**
 * User Schema
 */
var UserSchema = new Schema({
    username: {
        type: String,
        trim: true,
        unique: true,
        index: true,
        required : constants.messages.USER_NAME_REQUIRED
    },
    name: {
        type: String,
        trim: true
    },
    user_id :{
        type: Number,
        unique: true
    },
    user_info : {
        type: Schema.Types.Mixed
    },
    accessToken : {
        type: String,
        trim: true,
        index: true,
        default : ''
    },
    joinedOn :{
        type: Date,
        default: Date.now
    },
    sessionId : {
        type: String,
        trim: true,
        unique: true,
        index: true,
        default : ''
    }
});

// Export the Mongoose model
var User = mongoose.model('User', UserSchema);

module.exports.userModel = User;


