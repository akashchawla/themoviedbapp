'use strict';

/**
 * Module dependencies.
 */
var searchCtrl = require('../controllers/search.server.controller.js'),
    authCtrl = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {

    app.route('/api/v1/search')
        .get(authCtrl.tokenAuthentication, searchCtrl.searchResults);
};

