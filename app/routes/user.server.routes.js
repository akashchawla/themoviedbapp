'use strict';

/**
 * Module dependencies.
 */
var userCtrl = require('../controllers/user.server.controller.js'),
    authCtrl = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {

    app.route('/api/v1/users/signin')
        .post(authCtrl.signInAuthentication, userCtrl.signIn);
};

